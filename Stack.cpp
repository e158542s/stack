//Erica Sims
//ECSC 330
//Implementing a Stack HW

#include <iostream>
#include "Stack.h"
#include <stdexcept>
using namespace std;


//delete nodes
void FreeNodes(StackNodeT* top) {
	StackNodeT* temp{ top };
	while (temp != nullptr) {
		top = top->link;
		delete temp;
		temp = top;
	}
}
//copy nodes
void CopyNodes(StackNodeT* fromTop, StackNodeT*& toTop) {
	StackNodeT* rhsCurrent{ fromTop };
	StackNodeT* lhsCurrent{ nullptr };
	if (fromTop != nullptr) {
		toTop = new StackNodeT;
		toTop->data = rhsCurrent->data;
		rhsCurrent = rhsCurrent->link;
		lhsCurrent = toTop;
		while (rhsCurrent) {
			lhsCurrent->link = new StackNodeT;
			lhsCurrent = lhsCurrent->link;
			lhsCurrent->data = rhsCurrent->data;
			rhsCurrent = rhsCurrent->link;
		}
		// last is null
		lhsCurrent->link = nullptr;
	}
	
}


//constructor
StackT::StackT() : size(0), stack(nullptr) {
}
//copy constructor
StackT::StackT(const StackT& rhs) :
	stack(nullptr),
	size(rhs.size)
{
	
	CopyNodes(rhs.stack, stack);


}
//move copy constructor
StackT::StackT(StackT&& other) noexcept {
	size = std::exchange(other.size, 0);
	stack = std::exchange(other.stack, nullptr);
}

//destructor
StackT::~StackT(){
	FreeNodes(stack);
}
//asignment operator
StackT& StackT::operator=(const StackT& rhs) {
	if (this != &rhs) {
		FreeNodes(stack);
		// Re-initialize
		stack = nullptr;
		//entryNode = nullptr;
		size = rhs.size;

		CopyNodes(rhs.stack,stack);
	}
	return *this;
}

//push
void StackT::Push(ItemT newItem) {

	//create stack node
	StackNodeT* newNode = new StackNodeT;
	newNode->link = nullptr;
	newNode->data = newItem;

	//place stack node into stack
	
	if (!IsEmpty()) {
		newNode->link = stack;
	}
	
	stack = newNode;
	
	//update size
	size += 1;
	
}
//pop
void StackT::Pop() {
	try {
		if (stack == nullptr) {
			throw logic_error("pop called with empty stack");
		}
		else {


			StackNodeT* temp = stack;
			stack = stack->link;


			delete temp;
			size -= 1;
		}
	}
	catch (const logic_error& le) {
		cout << "Logic error: " << le.what() << endl;
	}
}
//top
ItemT StackT::Top() const{
	try {
		if (stack == nullptr) {
			throw logic_error("top called with empty stack");
			
		
		}
		else {
			return stack->data;
		}
	}
	catch (const logic_error& le) {
		cout << "Logic error: " << le.what() << endl;
	}
}
//isEmpty
bool StackT::IsEmpty()const {
	return(size == 0);
}
//size
size_t StackT::Size() const {
	return(size);
}