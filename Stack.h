#ifndef STACK
#define STACK
#include <cstddef>

using ItemT = int;


struct StackNodeT;

struct StackNodeT {
    ItemT data;
    StackNodeT* link;
};

class StackT {
public:
    StackT();
    StackT(const StackT&);

    StackT(StackT&& other) noexcept;

    ~StackT();
    StackT& operator=(const StackT&);

    void Push(ItemT);
    void Pop();
    ItemT Top() const;

    size_t Size() const;

    bool IsEmpty() const;

private:
    size_t size;
    StackNodeT* stack;
};

#endif