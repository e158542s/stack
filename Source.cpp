
#include <iostream>
#include "Stack.h"

using namespace std;

int main() {


	StackT mystack;

	mystack.Push(1);
	cout << mystack.Top() << endl;
	mystack.Push(2);
	StackT s =mystack;
	cout << mystack.Top() << endl;
	cout <<"size: " << mystack.Size() << endl;
	mystack.Push(3);

	cout << mystack.Top() << endl;
	cout << "size: " << mystack.Size() << endl;
	mystack.Pop();
	cout << mystack.Top() << endl;
	cout << "size: " << mystack.Size() << endl;
	mystack.Pop();
	cout << mystack.Top() << endl;
	cout << "size: " << mystack.Size() << endl;
	mystack.Pop();
	cout << mystack.Top() << endl;
	cout << "size: " << mystack.Size() << endl;
	cout << "____________" << endl;
	cout << s.Top() << endl;
	s.Push(4);
	cout << s.Top() << endl;
	s.Pop();
	cout << s.Top() << endl;


}